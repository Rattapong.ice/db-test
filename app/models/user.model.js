var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var crypto = require('crypto');

var UserSchema = new Schema({
    firstName: {type: String,required: true},
    lastName: {type: String,required: true},
    username: {type: String, unique: true,trim: true,required: true},
    email: {type: String, index: true,match:/.+\@.+\.+/},
    password: {type: String,required: true},
    created: {
        type: Date,
        default: Date.now
    },
    
});
UserSchema.pre('save', function (next) {
	if (this.password) {
		this.salt = new Buffer(crypto.randomBytes(16).toString('base64'), 'base64');
		this.password = this.hashPassword(this.password);
	}
	next();
});

UserSchema.methods.hashPassword = function (password) {
	return crypto.pbkdf2Sync(password, this.salt, 10000, 64, 'sha1').toString('base64');
};

UserSchema.methods.authenticate = function (password) {
	return this.password === this.hashPassword(password);
};
mongoose.model('User', UserSchema);